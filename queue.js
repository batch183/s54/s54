let collection = [];

// Write the queue functions below.
// Note: Avoid using Array methods (except .length) on creating the queue functions.

function print(){
	return collection;
}

function enqueue(collections){
	collection.push(collections);
	return collection;
}

function dequeue(collections){
	collection.shift();
	return collection;
}

function front(collections){
	return collection[0];
}

function size(collections){
	return collection.length;
}

function isEmpty(){
	return (size()===0);
}

module.exports = {
	print,enqueue,dequeue,front,size,isEmpty//export created queue functions
};